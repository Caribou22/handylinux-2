Contributions
=============

sources "de dépôt"
------------------
les sources du build HandyLinux ne sont pas des sources de développement : elles sont le dépôt public des scripts et fichiers utilisés pour la création de la distribution [HandyLinux](https://handylinux.org).

si vous désirez contribuer à la traduction, au code, aux tests, merci de venir sur [notre forum](https://handylinux.org/forum).


MeMo pour les sources HandyLinux
================================

sources Libres et publiques
---------------------------
les sources du build HandyLinux doivent rester libres et publiques afin de respecter la licence principale de la distribution, GPLv3

sources opérationnelles
-----------------------
- les sources du build HandyLinux sont publiques et doivent rester opérationnelles afin de permettre un clonage et une contribution efficace
- les tests sont à réaliser en local ou sur un clone du dépôt avant intégration


MeMo pour les releases
======================

ToDo avant chaque build d'une release
-------------------------------------
- attendre 4 jours minimum après la sortie de la release Debian afin de laisser 
  aux dev le temps de mettre à jour leurs applications et résoudre les petits bugs 
- mise à jour de la [config Firefox](/config/includes.chroot/etc/skel/.mozilla/firefox)
- mise à jour de la documentation et des paquets associés si nécessaire (handylinux-doc) 
- mise à jour du numéro de version dans les différents scripts preseed : /addons/32/handylinux_preseed* + /addons/64/handylinux_preseed* 
- mise à jour du handylinux-desktop.deb

ToDo après chaque sortie de release
-----------------------------------
- annonce officielle sur le [blog](http://blog.handylinux.org) avec le [Changelog](CHANGELOG.md) et les modifications annexes si nécessaire
- bandeau-annonce sur le [forum](https://handylinux.org/forum) avec lien vers le blog.
- mise à jour des liens et pages officielles :
  - liens de téléchargements sur la [page d'accueil](https://handylinux.org)
  - liens de téléchargements/torrents/md5 sur les pages wiki.handy [fr](https://handylinux.org/wiki/doku.php/fr/download) / [en](https://handylinux.org/wiki/doku.php/en/download)
  - n° et date de version handylinux sur [Debian Derivative](https://wiki.debian.org/Derivatives/Census/HandyLinux)
  - n° et date de version sur les pages [wikipédia-fr](https://fr.wikipedia.org/wiki/HandyLinux) + [wikipedia-en](https://en.wikipedia.org/wiki/HandyLinux)
  - n° de version + mini-changelog à envoyer sur la page [softpédia](http://linux.softpedia.com/get/System/Operating-Systems/Linux-Distributions/handylinux-102730.shtml)
- souffler un peu :)

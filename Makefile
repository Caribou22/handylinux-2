# handylinux makefile arnault perret <arpinux@member.fsf.org>

all: 
	@echo "Usage: as root"
	@echo "make 32b       : build handylinux2 i386"
	@echo "make 64b       : build handylinux2 amd64"
	@echo "make clean32b  : clean up i386 build directories"
	@echo "make clean64b  : clean up amd64 build directories"
	@echo "make cleanfull : clean up log & cache directories"

32b:
	@echo "-------------------------"
	@echo "building HandyLinux2 i386"
	@echo "-------------------------"
	cp auto/config32 auto/config
	cp addons/32/live32-en.cfg config/includes.binary/isolinux/live-en.cfg
	cp addons/32/live32.cfg config/includes.binary/isolinux/live.cfg
	cp addons/32/586-kernel-cleaner config/includes.chroot/usr/local/bin/586-kernel-cleaner
	cp addons/32/handylinux_preseed config/includes.chroot/usr/local/bin/handylinux_preseed
	cp addons/32/handylinux_preseed-en config/includes.chroot/usr/local/bin/handylinux_preseed-en
	cp addons/32/handylinux_preseed-access config/includes.chroot/usr/local/bin/handylinux_preseed-access
	cp addons/32/handylinux_preseed-access-en config/includes.chroot/usr/local/bin/handylinux_preseed-access-en
	cp addons/32/rc.local.kc config/includes.chroot/usr/share/handylinux/rc.local.kc
	cp addons/32/rc.local.orig config/includes.chroot/usr/share/handylinux/rc.local.orig
	lb build
	
64b:
	@echo "--------------------------"
	@echo "building HandyLinux2 amd64"
	@echo "--------------------------"
	cp auto/config64 auto/config
	cp addons/64/live64-en.cfg config/includes.binary/isolinux/live-en.cfg
	cp addons/64/live64.cfg config/includes.binary/isolinux/live.cfg
	cp addons/64/handylinux_preseed config/includes.chroot/usr/local/bin/handylinux_preseed
	cp addons/64/handylinux_preseed-en config/includes.chroot/usr/local/bin/handylinux_preseed-en
	cp addons/64/handylinux_preseed-access config/includes.chroot/usr/local/bin/handylinux_preseed-access
	cp addons/64/handylinux_preseed-access-en config/includes.chroot/usr/local/bin/handylinux_preseed-access-en
	lb build

clean32b:
	@echo "--------------------------"
	@echo "cleaning build directories"
	@echo "--------------------------"
	rm auto/config
	rm config/includes.binary/isolinux/live.cfg
	rm config/includes.binary/isolinux/live-en.cfg
	rm config/includes.chroot/usr/local/bin/handylinux_preseed*
	rm config/includes.chroot/usr/local/bin/586-kernel-cleaner
	rm config/includes.chroot/usr/share/handylinux/rc.local.kc
	rm config/includes.chroot/usr/share/handylinux/rc.local.orig
	lb clean

clean64b:
	@echo "--------------------------"
	@echo "cleaning build directories"
	@echo "--------------------------"
	rm auto/config
	rm config/includes.binary/isolinux/live.cfg
	rm config/includes.binary/isolinux/live-en.cfg
	rm config/includes.chroot/usr/local/bin/handylinux_preseed*
	lb clean

cleanfull:
	@echo "----------------------------------"
	@echo "cleaning build & cache directories"
	@echo "----------------------------------"
	rm -R -f cache


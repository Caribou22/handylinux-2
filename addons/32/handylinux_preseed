#!/bin/bash
# post-install script for handylinux :
#  - fix sources.list
#  - set installation version and date
#  - detect architecture
#  - detect and adjust kernel script needed
#  - purge en pkgs/files
#  - change handylinux doc rights
#  - set nice handy grub theme
#  - fix welcome launcher after installation
#  - fix usb installation entry in fstab (thx to Corenominal)
# arpinux <arpinux@member.fsf.org>
#############################################################

# set handylinux sources.list
mv -f /usr/share/handylinux/sources.list /etc/apt/sources.list

# set handylinux installation date and initial release"
echo "HandyLinux-2.5 `date +%a\ %d\ %b\ %Y`" > /etc/handylinux_installation

# check and clean language
# nettoyage de la doc anglaise
dpkg --purge handylinux-doc-en
# traduction de la demande de mot de passe
rm /etc/sudoers.d/hl-sudoers
mv /etc/sudoers.d/hl-sudoers-fr /etc/sudoers.d/hl-sudoers

# set appropriate kernel and delete useless one
cat /proc/cpuinfo | grep flags > paeornot
if grep -q pae paeornot; then
    #kernel pae : 586 kernel removed with post-install kernel-cleaner
    mv /usr/share/handylinux/rc.local.kc /etc/rc.local
else
    #kernel non-pae : removal of the 686-pae kernel & uneeded scripts
    rm /usr/share/handylinux/rc.local.kc
    rm /usr/share/handylinux/rc.local.orig
    rm /usr/local/bin/586-kernel-cleaner
    apt-get autoremove --purge -y linux-image-686-pae linux-image-3.16.0-4-686-pae
fi
rm paeornot

# ajouter un background à GRUB
echo "GRUB_BACKGROUND=\"/usr/share/images/grub/handylinux.tga\"" >> /etc/default/grub

# prise en compte du fond pour GRUB et du noyau conservé
update-grub

# changement du lanceur d'accueil pour les utilisateurs supplémentaires
sed -i s/me.sh/me2.sh/g /etc/skel/.config/autostart/welcome.desktop

# This is a fugly hack for fixing fstab after installing
# CrunchBang using unetbootin. Basically, if using unetbootin,
# the USB device is identified as a cdrom drive and added to
# /etc/fstab as that. This script will find any such entries
# in fstab and disable them. It is looking for cdrom entries
# which reference devices under "/dev/sdX".

# a big thank you to corenominal, dev of CrunchbangLinux

FSTAB=/etc/fstab
if grep "^/dev/sd" ${FSTAB} | grep "/media/" | grep "auto"
then
    NEEDLE=`grep "^/dev/sd" ${FSTAB} | grep "/media/" | grep "auto"`
    if ! echo "${NEEDLE}" | grep "#/dev/sd"
    then
        CORK="#${NEEDLE}"
        rpl -q "${NEEDLE}" "${CORK}" ${FSTAB}
    fi
fi
exit 0

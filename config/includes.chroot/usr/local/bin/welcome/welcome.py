#!/usr/bin/env python3
# -*- coding:Utf-8 -*- 


"""

Auteur :      thuban <thuban@yeuxdelibad.net>  
licence :     GNU General Public Licence v3

Description :
    message d'accueil à handylinux
"""

from gi.repository import Gtk, Gdk, GObject, GdkPixbuf 
import locale
import os
from textwrap import fill

# Trads
LOCALE = locale.setlocale(locale.LC_ALL, "")[0:2]

appname = "Welcome to HandyLinux"
msg1 = "For absolute beginners, left-click here."
msg2 = "To access main available applications on your computer, left-click on the blue handymenu icon on bottom left corner of your screen."
msg3 = "To access main online documentation or the specific accessibility tools, left-click on the access menu."
msg4 = "To shutdown or reboot your computer, left-click on the red icon on bottom right corner of your screen."
endmsg = "Have fun with HandyLinux! "
doclink="file:///usr/share/handylinux/guide-en.html"


if LOCALE == 'fr':
    appname = "Bienvenue sur HandyLinux"
    msg1 = "Si vous débutez en informatique, clic-gauche ici."
    msg2 = "Pour accéder aux principales applications disponibles sur votre machine, cliquez gauche sur l'icône handymenu en bas à gauche de votre écran."
    msg3 = "Pour accéder à l'aide en ligne et aux outils d'accessibilité, clic-gauche sur le menu access en bas à droite."
    msg4 = "Pour éteindre ou redémarrer votre ordinateur, clic-gauche sur l'icône appropriée en bas à droite de l'écran."
    endmsg = "Amusez-vous bien avec HandyLinux ! "
    doclink="file:///usr/share/handylinux/guide.html"


top = "<span size='30000'>{}</span>".format(appname)

class Welcome():
    def __init__(self):
        # window
        self.window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
        self.window.connect("delete_event", lambda x,y: Gtk.main_quit())
        self.window.set_title(appname)
        self.window.set_position(Gtk.WindowPosition.CENTER_ALWAYS)

        # main box
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box.set_homogeneous(False)
        box.set_border_width(20)


        # text at top
        label = Gtk.Label()
        label.set_justify(Gtk.Justification.LEFT)
        label.set_markup(top)
        box.pack_start(label, True, True, 10)


        # first message for beginners
        btn1 = Gtk.Button(msg1)
        btn1.set_relief(Gtk.ReliefStyle.NONE)
        btn1.set_tooltip_text(msg1)
        btn1.connect("button_release_event", lambda x,y: os.system("x-www-browser {} & ".format(doclink)))
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)

        # image left-click
        img = Gtk.Image.new_from_file("/usr/share/handylinux/pics/mouse-left.png")
        ebox = Gtk.EventBox()
        ebox.connect("button_release_event", lambda x,y: os.system("x-www-browser {} &".format(doclink)))
        ebox.add(img)

        hbox.pack_start(btn1, True, True, 1)
        hbox.pack_start(ebox, False, False, 1)
        box.pack_start(hbox, True, True, 1)



        # second message for handymenu
        btn = Gtk.Button(fill(msg2, 60))
        btn.set_relief(Gtk.ReliefStyle.NONE)
        btn.set_tooltip_text(msg2)
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)

        img = Gtk.Image.new_from_icon_name("handylinux", Gtk.IconSize.DIALOG)

        hbox.pack_start(img, False, False, 1)
        hbox.pack_start(btn, True, True, 1)
        box.pack_start(hbox, True, True, 1)



        # message access
        btn = Gtk.Button(fill(msg3, 60))
        btn.set_relief(Gtk.ReliefStyle.NONE)
        btn.set_tooltip_text(msg3)
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)

        img = Gtk.Image.new_from_icon_name("access", Gtk.IconSize.DIALOG)

        hbox.pack_start(btn, True, True, 1)
        hbox.pack_start(img, False, False, 1)
        box.pack_start(hbox, True, True, 1)



        # message shutdown
        btn = Gtk.Button(fill(msg4, 60))
        btn.set_relief(Gtk.ReliefStyle.NONE)
        btn.set_tooltip_text(msg4)
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)

        img = Gtk.Image.new_from_icon_name("system-shutdown", Gtk.IconSize.DIALOG)

        hbox.pack_start(img, False, False, 1)
        hbox.pack_start(btn, True, True, 1)
        box.pack_start(hbox, True, True, 1)



        # exit button
        exitbtn = Gtk.Button.new_with_mnemonic(endmsg)
        exitbtn.connect("button_release_event", lambda x,y: Gtk.main_quit())
        exitbtn.set_alignment(1,0.5)
        box.pack_start(exitbtn, False, False, 0)


        self.window.add(box)
        self.window.show_all()

def main():
    w = Welcome()
    Gtk.main()


    return 0

if __name__ == '__main__':
	main()


# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

